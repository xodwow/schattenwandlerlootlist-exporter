import got from "got"
import fs from "fs"
import JSZip from "jszip"
import {Class} from "./Class"
import {parseSpec} from "./parseSpec"
import {throwError} from "./throwError"

const SHEET_ID = "1iDM688cfQtvLSyh8e0DpChPzwzCySzBevXr_cjgngyY"
const DOWNLOAD_URL = `https://docs.google.com/spreadsheets/d/${SHEET_ID}/export?format=zip&id=${SHEET_ID}`

export async function exportSheetData() {
    const {body} = await got(DOWNLOAD_URL, {responseType: "buffer"})
    fs.writeFileSync("dl.zip", body)
    // const body = fs.readFileSync("dl.zip")
    const zip = await JSZip.loadAsync(body)

    const players = (await Promise.all([
        await parseSpec(Class.DRUID, await fileInsideZip(zip, "Druide.html")),
        await parseSpec(Class.WARLOCK, await fileInsideZip(zip, "Hexenmeister.html")),
        await parseSpec(Class.HUNTER, await fileInsideZip(zip, "Jäger.html")),
        await parseSpec(Class.MAGE, await fileInsideZip(zip, "Magier.html")),
        await parseSpec(Class.PRIEST, await fileInsideZip(zip, "Priester.html")),
        await parseSpec(Class.ROGUE, await fileInsideZip(zip, "Schurke.html")),
        await parseSpec(Class.SHAMAN, await fileInsideZip(zip, "Schamane.html")),
        await parseSpec(Class.WARRIOR, await fileInsideZip(zip, "Krieger.html")),
    ])).flat()

    return players
}

async function fileInsideZip(zip: JSZip, fileName: string) {
    return await (zip.file(fileName) || throwError(`${fileName} missing`)).async("string")
}
