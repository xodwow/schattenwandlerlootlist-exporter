export function throwError(str: string): never {
    throw new Error(str)
}
