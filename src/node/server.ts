import Koa from "koa"
import send from "koa-send"
import compress from "koa-compress"
import {exportSheetData} from "../exportSheetData"
import {luaifyPlayers} from "../luaifyPlayers"

new Koa()
    .use(compress())
    .use(handleExport)
    .use(ctx => send(ctx, ctx.path, {
        root: "build/dist",
        index: "index.html",
        setHeaders(res, path) {
            if (!path.endsWith("/index.html"))
                res.setHeader("Cache-Control", `public, max-age=${30 * 24 * 60 * 60}, immutable`)
        }
    }))
    .listen(process.env.PORT || 5000)

async function handleExport(ctx: Koa.Context, next: Koa.Next) {
    if (ctx.url !== "/export")
        return next()

    const players = await exportSheetData()
    if (process.env.NODE_ENV !== "production")
        ctx.set("Access-Control-Allow-Origin", "*")

    if (ctx.request.get("Accept") === "application/json")
        ctx.body = players
    else {
        ctx.response.attachment("import.lua")
        ctx.body = luaifyPlayers(players)
    }
}
