import {calculateStats} from "../calculateStats"
import {exportSheetData} from "../exportSheetData"
import {luaifyPlayers} from "../luaifyPlayers"

(async function () {
    const data = await exportSheetData()
    console.log(luaifyPlayers(data))
    console.error(calculateStats(data).str.join("\n"))
})()
