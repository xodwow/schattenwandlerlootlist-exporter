import {PlayerLootData} from "./PlayerLootData"

export function calculateStats(playerLootData: PlayerLootData[]) {
    const numPlayers = playerLootData.length
    const playersWithoutData = playerLootData
        .filter(player => !player.slots.filter(slot => slot && (slot[2].length || slot[3].length)).length)
        .map(player => player.name)

    const slotsWithData = playerLootData.map(player => player.slots.filter(isDefined)).flat()
    const totalMainListItems = slotsWithData.map(slotData => slotData[2].length).reduce(sum, 0)
    const totalAltListItems = slotsWithData.map(slotData => slotData[3].length).reduce(sum, 0)
    const totalListItems = totalMainListItems + totalAltListItems
    const totalFilledSlots = slotsWithData.map(slots => slots[0]).filter(Boolean).length

    const str = [
        `${numPlayers} Spieler exportiert mit ${totalFilledSlots} bereits gefüllten Plätzen von ${totalMainListItems} und ${totalAltListItems} Gegenständen auf den Haupt- und Alternativ-Listen.`,
        `Spieler ohne Daten: ${playersWithoutData.join(", ") || "-"}`,
    ]

    return {numPlayers, playersWithoutData, totalMainListItems, totalAltListItems, totalListItems, totalFilledSlots, str}
}

function sum(previous: number, current: number) {
    return previous + current
}

function isDefined<T>(obj: T | undefined | null): obj is T {
    return obj !== undefined && obj !== null;
}
