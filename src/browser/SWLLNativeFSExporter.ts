import {get, set} from "idb-keyval"
import {css, customElement, html, LitElement, property} from "lit-element"
import {calculateStats} from "../calculateStats"
import {luaifyPlayers} from "../luaifyPlayers"
import {PlayerLootData} from "../PlayerLootData"


@customElement("swll-native-fs-exporter")
export class SWLLNativeFSExporter extends LitElement {

    static readonly nativeFsSupported = !!window.showSaveFilePicker
    @property() inProgress = false
    @property() status: string | undefined

    static get styles() {
        return css`
            pre {
                white-space: pre-line;
            }
        `
    }

    render() {
        return html`
            <h2>Direktes Update</h2>
            <p>
                Der Browser unterstützt direktes Bearbeiten der Datei, damit können die Daten mit einem Klick
                aktualisiert werden.
            </p>
            <ol>
                <li><button type="button" ?disabled=${this.inProgress} @click=${this.writeDirectly}>Aktualisieren</button> betätigen</li>
                <li>die <code>World of Warcraft/_classic_/Interface/AddOns/SchattenwandlerLootList/import.lua</code>-Datei auswählen bzw. den Zugriff darauf erlauben</li>
                <li>im Spiel das Interface neu laden und mit <code>/swll sync</code> die neuen Daten veröffentlichen</li>
            </ol>
            ${this.status ? html`<pre>${this.status}</pre>` : ""}
        `
    }

    async openFile() {
        const key = "import-lua"
        const storedDirectory = await get<FileSystemFileHandle>(key)
        if (storedDirectory) {
            const options = {writable: true, mode: "readwrite"} as const
            if (await storedDirectory.queryPermission(options) === "granted")
                return storedDirectory
            if (await storedDirectory.requestPermission(options) === "granted")
                return storedDirectory

            throw new Error("Permission not granted")
        }
        const file = await window.showSaveFilePicker({
            types: [{accept: {"text/x-lua": [".lua"]}}],
            excludeAcceptAllOption: true,
        })
        if (file.name !== "import.lua")
            throw new Error("Invalid file, please select the SchattenwandlerLootList/import.lua file")
        await set(key, file)
        return file
    }

    async writeDirectly() {
        this.inProgress = true

        try {
            this.status = "Öffne Datei..."
            const file = await this.openFile()

            this.status = "Lade neue Daten..."
            const init = {headers: {"Accept": "application/json"}}
            const url = process.env.BACKEND_URL!
            const players = await fetchOrThrow<PlayerLootData[]>(url, init)
            const lua = luaifyPlayers(players)

            this.status = "Speichere..."
            const writable = await file.createWritable()
            await writable.write(lua)
            await writable.close()
            this.status = [
                `${new Date().toLocaleTimeString()}: Update erfolgreich.`,
                ...calculateStats(players).str
            ].join("\n")
        } catch (e) {
            this.status = `Update gescheitert (${e.message})`
            throw e
        } finally {
            this.inProgress = false
        }
    }

}

async function fetchOrThrow<T>(url: string, init?: RequestInit) {
    const response = await fetch(url, init)
    if (!response.ok)
        throw new Error(response.statusText)
    return await response.json() as T
}
