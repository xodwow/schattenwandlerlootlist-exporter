import {css, customElement, html, LitElement} from "lit-element"
import "./SWLLFileDownloadExporter"
import {SWLLNativeFSExporter} from "./SWLLNativeFSExporter"

@customElement("swll-exporter")
export class SWLLExporter extends LitElement {
    static get styles() {
        return css`
            main {
                padding: 2em;
                box-shadow: 0 0 1em rgba(0, 0, 0, 0.3);
                background: #eee;
            }
            h1 {
                margin: 0;
            }
        `
    }

    render() {
        return html`
            <main>
                <h1>SchattenwandlerLootList Datenexport</h1>
                <p>
                    Hiermit kann die interne Datenbank des <a href="https://gitlab.com/xodwow/schattenwandlerlootlist/-/blob/master/README.md">SchattenwandlerLootList-Addons</a> direkt mittels des Google Docs Sheets aktualisiert werden.
                </p>
                ${SWLLNativeFSExporter.nativeFsSupported ? html`<swll-native-fs-exporter></swll-native-fs-exporter>` : ""}
                <swll-file-download-exporter></swll-file-download-exporter>
            </main>
        `
    }
}
