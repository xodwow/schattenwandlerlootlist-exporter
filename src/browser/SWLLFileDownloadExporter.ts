import {customElement, html, LitElement} from "lit-element"

@customElement("swll-file-download-exporter")
export class SWLLFileDownloadExporter extends LitElement {
    render() {
        return html`
            <h2>Download</h2>
            <ol>
                <li><a href=${process.env.BACKEND_URL}><code>import.lua</code></a> herunterladen</li>
                <li>im <code>World of Warcraft/_classic_/Interface/AddOns/SchattenwandlerLootList/</code>-Verzeichnis die alte Datei überschreiben</li>
                <li>im Spiel das Interface neu laden und mit <code>/swll sync</code> die neuen Daten veröffentlichen</li>
            </ol>
        `
    }
}
