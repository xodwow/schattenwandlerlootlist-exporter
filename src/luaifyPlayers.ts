import {luaify} from "./luaify"
import {PlayerLootData} from "./PlayerLootData"

export function luaifyPlayers(players: PlayerLootData[]) {
    return "SWLL.IMPORT = " + luaify({version: 4, timestamp: new Date().toISOString(), players})
}
