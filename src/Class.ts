export enum Class {
    DRUID = "DRUID",
    HUNTER = "HUNTER",
    MAGE = "MAGE",
    PRIEST = "PRIEST",
    ROGUE = "ROGUE",
    SHAMAN = "SHAMAN",
    WARLOCK = "WARLOCK",
    WARRIOR = "WARRIOR",
}
