const INDENTATION = "  "

export function luaify(obj: unknown, indent = ""): string {
    if (typeof obj === "boolean" || typeof obj === "string" || typeof obj === "number")
        return JSON.stringify(obj)

    if (obj === undefined || obj === null)
        return "nil"

    const outerIndent = indent
    indent += INDENTATION

    if (Array.isArray(obj)) {
        if (!obj.length)
            return "{}"
        let str = "{\n"
        for (const v of obj)
            str += indent + `${luaify(v, indent)},\n`
        str += outerIndent + "}"
        return str
    }

    if (typeof obj === "object" && obj !== null) {
        let str = "{\n"
        for (const [k, v] of Object.entries(obj))
            str += indent + `[${luaify(k)}] = ${luaify(v, indent)},\n`
        str += outerIndent + "}"
        return str
    }

    throw new Error("unsupported object type")
}
