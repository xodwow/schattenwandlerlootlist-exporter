import {Class} from "./Class"
import {SlotState} from "./SlotState"

export interface PlayerLootData {
    name: string
    class: Class
    slots: Array<[SlotState, SlotState, number[], number[]] | undefined>
}
