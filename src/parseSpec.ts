import cheerio from "cheerio"
import {Class} from "./Class"
import {PlayerLootData} from "./PlayerLootData"
import {SlotState} from "./SlotState"
import {throwError} from "./throwError"

const NAME_REGEX = /^\p{Letter}+/u
const ITEM_LINK_REGEX = /^https:\/\/(?:de\.)?classic\.wowhead\.com\/item=([0-9]+)/

export async function parseSpec(playerClass: Class, html: string): Promise<PlayerLootData[]> {
    const $ = cheerio.load(html)
    const matrix = parseTable($)
    const [hasItemsOnMainList, hasItemsOnAlternativeList] = loadReceivedItems(matrix, $)
    const [mainLists, alternativeLists] = loadWishItems(matrix, $)
    const players: PlayerLootData[] = []
    const playerNames = new Set([
        ...hasItemsOnMainList.keys(),
        ...hasItemsOnAlternativeList.keys(),
        ...mainLists.keys(),
        ...alternativeLists.keys(),
    ])
    for (const name of playerNames) {
        const data: PlayerLootData = {
            name,
            class: playerClass,
            slots: Array(18).fill(true).map((_, slotIndex) => {
                const hasMainItem = hasItemsOnMainList.get(name)?.[slotIndex]
                    ?? throwError(`${name} missing in main item list`)
                const hasAlternativeItem = hasItemsOnAlternativeList.get(name)?.[slotIndex]
                    ?? throwError(`${name} missing in alternative item list`)
                const mainList = mainLists.get(name)?.[slotIndex]
                    ?? throwError(`${name} missing in main wish list`)
                const alternativeList = alternativeLists.get(name)?.[slotIndex]
                    ?? throwError(`${name} missing in alternative wish list`)
                return [hasMainItem, hasAlternativeItem, Array.from(mainList), Array.from(alternativeList)]
            }),
        }
        players.push(data)
    }
    players.sort((p1, p2) => p1.name.localeCompare(p2.name))
    return players
}

function parseTable($: cheerio.Root): cheerio.Element[][] {
    const matrix = $("tbody tr").toArray().map(row => $(row).find("td:not(.freezebar-cell)").toArray())
    const visited = new Set<cheerio.Element>()
    matrix.forEach((tr, trIndex) => {
        tr.forEach((td, tdIndex) => {
            if (visited.has(td))
                return
            else
                visited.add(td)

            for (let r = 0; r < rowspanNumber(td); r++)
                for (let c = 0; c < colspanNumber(td); c++)
                    if (matrix[trIndex + r][tdIndex + c] !== td)
                        matrix[trIndex + r].splice(tdIndex + c, 0, td)
        })
    })

    return matrix
}

function loadReceivedItems(matrix: cheerio.Element[][], $: cheerio.Root) {
    const hasItemOnMainList = new Map<string, SlotState[]>()
    const hasItemOnAlternativeList = new Map<string, SlotState[]>()
    const rows = matrix.filter(tr => $(tr[1]).text().match(/^[0-9]+$/))
    for (const tr of rows) {
        const name = getName(tr)
        const hasItemInSlots = tr.slice(2, 20).map(td => getSlotState($(td).text()))
        if (hasItemOnMainList.has(name))
            hasItemOnAlternativeList.set(name, hasItemInSlots)
        else
            hasItemOnMainList.set(name, hasItemInSlots)
    }
    return [hasItemOnMainList, hasItemOnAlternativeList]
}

function loadWishItems(matrix: cheerio.Element[][], $: cheerio.Root) {
    const mainSlotsPerPlayer = new Map<string, Set<number>[]>()
    const alternativeSlotsPerPlayer = new Map<string, Set<number>[]>()

    const rows = matrix.filter(tr => $(tr[1]).text().match(/^[AH]$/))
    for (const tr of rows) {
        const name = getName(tr)
        const main = $(tr[1]).text() === "H"
        const itemIDs = tr.slice(2, 20).map((td, tdIndex) => {
            const a = $(td).find("a")
            if (!a.length)
                return
            const href = a.attr("href")
                ?? throwError(`Link without href, name: ${name}, col: ${columnChar(2 + tdIndex)}, text: ${a.text()}`)
            const [, itemID] = ITEM_LINK_REGEX.exec(href)
                ?? throwError(`Unexpected link, name: ${name}, col: ${columnChar(2 + tdIndex)}, href: ${href}`)
            return href && Number.parseInt(itemID) || undefined
        })
        const slotsPerPlayer = main ? mainSlotsPerPlayer : alternativeSlotsPerPlayer
        if (!slotsPerPlayer.has(name)) {
            const slots = Array<Set<number>>(18)
            for (let i = 0; i < slots.length; i++)
                slots[i] = new Set<number>()
            slotsPerPlayer.set(name, slots)
        }
        const slots = slotsPerPlayer.get(name)!
        itemIDs.forEach((itemID, slotIndex) => {
            if (itemID)
                slots[slotIndex].add(itemID)
        })
    }
    return [mainSlotsPerPlayer, alternativeSlotsPerPlayer]
}

function columnChar(i: number) {
    return String.fromCharCode("A".charCodeAt(0) + i)
}

function getSlotState(text: string) {
    if (text.toLowerCase() === "q")
        return SlotState.NON_MAINRAID
    return text
        ? SlotState.FILLED
        : SlotState.EMPTY
}

function getName(tr: cheerio.Element[]) {
    const firstLine = tr[0].firstChild.data || throwError("Name line in upper list is empty")
    const [name] = NAME_REGEX.exec(firstLine) || throwError("Name in upper list not found")
    return name
}

function colspanNumber(td: cheerio.Element) {
    const str = cheerio(td).attr("colspan")
    return str ? Number.parseInt(str) : 1
}

function rowspanNumber(td: cheerio.Element) {
    const str = cheerio(td).attr("rowspan")
    return str ? Number.parseInt(str) : 1
}
