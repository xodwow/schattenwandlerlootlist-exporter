declare const showDirectoryPicker: (opts?: {}) => Promise<FileSystemDirectoryHandle>
declare const showOpenFilePicker: (opts?: FilePickerOptions & { multiple?: boolean }) => Promise<FileSystemFileHandle[]>
declare const showSaveFilePicker: (opts?: FilePickerOptions) => Promise<FileSystemFileHandle>

declare interface Window {
    showDirectoryPicker: typeof showDirectoryPicker
    showOpenFilePicker: typeof showOpenFilePicker
    showSaveFilePicker: typeof showSaveFilePicker
}

declare interface FilePickerOptions {
    types?: FilePickerAcceptType[]
    excludeAcceptAllOption?: boolean
}

declare interface FilePickerAcceptType {
    description?: string
    accept?: Record<string, string | string[]>
}

declare interface FileSystemDirectoryHandle extends FileSystemHandle {
    getEntries(): AsyncIterableIterator<[string, FileSystemHandle]>

    /** @deprecated */
    getFile?(name: string, options?: { create?: boolean }): Promise<FileSystemFileHandle>

    getFileHandle(name: string, options?: { create?: boolean }): Promise<FileSystemFileHandle>

    /** @deprecated */
    getDirectory?(name: string, options?: { create?: boolean }): Promise<FileSystemDirectoryHandle>

    getDirectoryHandle(name: string, options?: { create?: boolean }): Promise<FileSystemDirectoryHandle>

    removeEntry(name: string, options?: { recursive?: boolean }): Promise<void>

    resolve(possibleDescendant: FileSystemHandle): Promise<string[] | undefined>
}

declare interface FileSystemFileHandle extends FileSystemHandle {
    getFile(): Promise<File>

    createWritable(options?: { keepExistingData?: boolean }): Promise<FileSystemWritableFileStream>
}

interface FileSystemHandlePermissionDescriptor {
    /** @deprecated */
    writable?: boolean
    mode: "read" | "readwrite"
}

declare interface FileSystemHandle {
    readonly kind: "file" | "directory"
    readonly name: string

    isSameEntry(other: FileSystemHandle): Promise<boolean>

    queryPermission(descriptor?: FileSystemHandlePermissionDescriptor): Promise<PermissionState>

    requestPermission(descriptor?: FileSystemHandlePermissionDescriptor): Promise<PermissionState>
}

declare interface WriteParams {
    type: "write" | "seek" | "truncate"
    size?: number
    position?: number
    data?: BufferSource | Blob | string
}

declare interface FileSystemWritableFileStream extends WritableStream {
    write(data: BufferSource | Blob | string | WriteParams): Promise<void>

    seek(position: number): Promise<void>

    truncate(size: number): Promise<void>
}

declare interface WritableStream<W = any> {
    close(): Promise<void>
}

